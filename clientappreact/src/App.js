
import './App.css';
import React from 'react';
import "ag-grid-community/dist/styles/ag-grid.css"
import 'ag-grid-community/dist/styles/ag-theme-balham.css'
import { AgGridReact } from 'ag-grid-react';
//import axios from 'axios';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }
// const instance = axios.create({
//   baseURL: 'http://localhost:8080',
//   headers: {'Access-Control-Allow-Origin':'*'}
  
// });
class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      columnsDefs:[
        {headerName:'id',field:'id', sortable:true, filter:true },
        {headerName:'name',field:'name', sortable:true, filter:true },
        {headerName:'apellido',field:'apellido', sortable:true, filter:true },
        {headerName:'Correo electronico',field:'email',sortable:true, filter:true}
      ],
      rowdata:null,
      // rowData:[
      //   {id:1,name:'ivan',apellido:'gaspar quispe',email:'elmascrack@gmail.com'},
      //   {id:1,name:'juan ivan',apellido:'gaspar quispe',email:'elmascrack61294@gmail.com'},
      // ]
    }
  }
  componentDidMount(){
   fetch('http://localhost:8080/user')
    .then(res=> res.json())
    .then(rowdata=>this.setState({rowdata})) 
    .catch(err=>console.log(err));
  }
  render(){
    return(
      <div
       className="ag-theme-balham"
       style={{
         width:850,
         height:600
       }}
      >
        <AgGridReact 
        pagination={true}
        paginationAutoPageSize={true}
        columnDefs={this.state.columnsDefs}
        rowData = {this.state.rowdata}
        />
      </div>
    ) }
}

export default App;
